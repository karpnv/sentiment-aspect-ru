# -*- coding: utf-8 -*-

import xml.etree.ElementTree as ET

class Yarn:
    def __init__(self):
        self.yarn_synonyms = self.load_synonyms('yarn.xml')
        print "Initialized"

    def load_synonyms(self, name):
        """Извлекает все синонимы из yarn.xml в словарь self.yarn_synonyms без учета синсетов"""
        tree = ET.parse(name)
        root = tree.getroot()
        words_list = {}
        synsets_list = []
        words = root.find('words')
        for wordEntry in words.findall('wordEntry'):
            words_list[wordEntry.get('id')] = wordEntry.find('word').text
        synsets = root.find('synsets')
        for synsetEntry in synsets.findall('synsetEntry'):
            synset = set()
            for word in synsetEntry.findall('word'):
                synset.add(word.get('ref'))
            synsets_list.append(synset)

        yarn_synonyms = {}
        for id, word in words_list.items():
            for synset in synsets_list:
                if id in synset:
                    yarn_synonyms[word] = set()
                    for syn_id in synset:
                        if id == syn_id:
                            continue
                        elif words_list.get(syn_id):
                            yarn_synonyms[word].add(words_list[syn_id])
        return yarn_synonyms

    def get_synonyms(self, word):
        '''Возврящает список синонимов для заданного слова'''
        return self.yarn_synonyms.get(word)