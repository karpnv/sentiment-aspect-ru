# -*- coding: utf-8 -*-

import re, pickle
from gensim import corpora, models, similarities
from Reg import RegExp
import pymorphy2
from feature_extractor import learn_terms, evaluate, read_test_answers, print_results, remove_subterms, submit_results
from yarn import Yarn

class AspectSentiment:
    aspcts={}

    def __init__(self, fileName=''):
        self.fileName=fileName
        self.morph=pymorphy2.MorphAnalyzer()
        self.reg=RegExp()
        self.reCompile = re.compile('[%s]' % re.escape(self.reg.punctuation.decode('utf-8'))) #'!"$%&\'()*+,-./:;<=>?@[\]^`{|}~«–“—…»☦№'.decode('utf-8')))
        self.stopwords=set([])
        for stopw in self.reg.stopwords:
            self.stopwords.add(stopw.decode('utf-8'))
        self.ya=Yarn()

    def read_txt_file(self, fileName=''):
        with open(self.fileName+'.txt', 'rb') as f:
            fd=f.read().decode('utf8')
        lines = fd.split("\n")
        texts=[]
        ids={}
        i=0
        for l in lines:
            pair=l.split('<SPLITTER>')
            texts.append(pair[1])
            ids[i]=int(pair[0])
            i=i+1
        return texts, ids

    def tokenize(self, texts, isSerialize=0):
        texts2=[]
        for text in texts:
            texts2.append(text)
        texts1=self.reg.replace(texts)
        out_texts=[]
        i=0
        for text in texts1:
            duplicate={}
            #print text
            translated = self.reCompile.sub('', text)
            t = translated.split()
            words=[]
            for word in t:
                pword = word.strip().lower()
                if pword not in self.stopwords:
                    try:
                        word_in_work=self.morph.parse(pword)[0]
                        #p = re.compile(pword)
                        #m = p.match(texts[i])
                        w_c=texts2[i].count(pword)
                        if  w_c > 1:
                            if duplicate.has_key(pword):
                                duplicate[pword]=duplicate[pword]+1
                            else:
                                duplicate[pword]=1
                            st=-1
                            for index in range(duplicate[pword]):
                                st=texts2[i].find(pword, st+1)
                                #print w_c, index, st, pword, duplicate[pword]
                        else:
                            st=texts2[i].find(pword)
                        if st!=-1:
                            words.append([word_in_work.normal_form.strip(), pword, st, st+len(pword)])
                            #print word_in_work.normal_form.strip(), pword, st, st+len(pword)
                    except:
                        pass
            out_texts.append(words)
            i=i+1
        if isSerialize!=0:
            with open(self.fileName+'.wordsPosition', 'w') as f:
                pickle.dump(out_texts, f)
        return out_texts


    def prepare_aspects(self):
        terms_dict = learn_terms('SentiRuEval_rest_markup.xml')
        b_list=[]
        for a_type in terms_dict:
            b_list=b_list+self.tokenize(terms_dict[a_type])
        aspect_1set=set([])
        aspect_more1_list=[]
        for aspect in b_list:
            if len(aspect)==1:
                aspect_1set.add(aspect[0][0])
            elif len(aspect)>1:
                aspect_more1_list.append(aspect)
        return aspect_1set, aspect_more1_list

    def match_aspects(self, aspect_1set, aspect_more1_list, isSerialize=0):
        with open(self.fileName+'.wordsPosition', 'rb') as f:
            texts=pickle.load(f)
        i=0
        detected_list=[]
        for text in texts:
            print i
            j=0
            for word in text:
                #if word[0].strip() in aspect_1set:
                #    print 'ф'.decode('utf-8')+wsk+word[0].strip()
                if word[0].strip() in aspect_1set:
                    detected_list.append([i,j,j+1])
                    #print [i,j,j+1]

                try:
                    for asp in aspect_more1_list:
                        flag=0
                        for k in range(len(asp)):
                            if asp[k][0].strip()==texts[i][j+k][0].strip():
                                #print len(asp), asp[k][0], k
                                flag=flag+1
                            else:
                                flag=0
                        if flag==len(asp):
                            detected_list.append([i,j,j+len(asp)])
                            #for k in range(len(asp)):
                            #    print asp[k][0]
                except:
                    pass
                j=j+1
            i=i+1
        if isSerialize!=0:
            with open(self.fileName+'.match_aspects', 'w') as f:
                pickle.dump(detected_list, f)
        return detected_list

    def get_asp_position(self, ids, isSerialize=0):
        with open(self.fileName+'.match_aspects', 'rb') as f:
            detected_list=pickle.load(f)
        with open(self.fileName+'.wordsPosition', 'rb') as f:
            out_texts=pickle.load(f)
        pos_set=set()
        for item in detected_list:
            i=0
            for w_num in range(item[1], item[2], 1):
                if i==0:
                    start=out_texts[item[0]][w_num][2]
                    fin=out_texts[item[0]][w_num][3]
                else:
                    fin=out_texts[item[0]][w_num][3]
                i=i+1
                #print out_texts[item[0]][w_num][0].strip(), start, fin
            pos_set.add((ids[item[0]], start, fin))
        if isSerialize!=0:
            with open(self.fileName+'.pos_set', 'w') as f:
                pickle.dump(pos_set, f)
        return pos_set

    def add_synonims(self, a_s):
        new_a_s=set()
        for asp in a_s:
            syn_set=self.ya.get_synonyms(asp)
            if syn_set!=None:
                for syn in syn_set:
                    new_a_s.add(syn)
                    #print 'syn=', syn
        a_s=a_s | new_a_s
        return a_s

if __name__ == '__main__':
    AS=AspectSentiment('_rest_test_texts')
    texts, ids=AS.read_txt_file()#_rest_test_texts _toy_test_texts
    AS.tokenize(texts, 1)
    a_s, a_l=AS.prepare_aspects()
    #a_s=AS.add_synonims(a_s)
    AS.match_aspects(a_s, a_l, 1)
    h=AS.get_asp_position(ids, 1)
    h_shortened=remove_subterms(h)
    y = read_test_answers('_rest_test_answers.csv')  # set of tuples of 3: (id, start, end)
    res_tup= evaluate(h_shortened, y)
    submit_results(h_shortened, AS.fileName+'.result.xml')
    print_results(*res_tup)