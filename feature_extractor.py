#! python2
# coding=utf-8

from __future__ import print_function
import codecs
import re
import xml.etree.ElementTree as ET
import treetaggerwrapper
tagger = treetaggerwrapper.TreeTagger(TAGLANG='ru', TAGDIR='TreeTagger', TAGPARFILE=r'TreeTagger\lib\russian.par',
                                      TAGINENC='utf-8', TAGOUTENC='utf-8')


# __GLOBALS____________________________________________________________________

CATEGORIES = {1: 'Food', 2: 'Interior', 3: 'Price', 4: 'Whole', 5: 'Service'}
CATEGORIES_REVERSE = {v: k for k, v in CATEGORIES.items()}


# __FUNCTIONS__________________________________________________________________


def annotate(train_file, test_reviews_file, test_answers_file):
    """
    Главная функция, которая "обучает" и использует baseline для разметки аспектных терминов
    в тестовой коллекции отзывов.
    :param train_file: название файла XML с размеченными отзывами (обучающие данные)
    :param test_reviews_file: название файла TXT с текстами, которые необходимо разметить
    :param test_answers_file: название файла CSV с "правильными ответами"
    :return: None (пока)
    """
    terms_dict = learn_terms(train_file)
    terms_set = flatten_term_dict(terms_dict)  # set of strings
    write_text(u'\n'.join(sorted(terms_set)), '_learned_terms.txt')
    x = read_test_reviews(test_reviews_file)  # list of tuples of 2: (id, text)
    y = read_test_answers(test_answers_file)  # set of tuples of 3: (id, start, end)
    with open('_y.txt', 'w') as f:
        f.write(str(y))
    h_base = find_terms_in_collection(x, terms_set)  # формат как y: set of tuples of 3: (id, start, end)
    with open('_h_base.txt', 'w') as f:
        f.write(str(h_base))
    h_base_super = remove_subterms(h_base)
    with open('_h_base_super.txt', 'w') as f:
        f.write(str(h_base_super))
    res_tup = evaluate(h_base_super, y)
    print_results(*res_tup)


def evaluate(h, y):
    """
    Оценивает результаты по метрикам precision/recall/F1
    :param h: гипотетические ответы (множество)
    :param y: правильные ответы (множество)
    :return: кортеж (P, R, F1)
    """
    tp_set = h & y
    fp_set = h - y
    fn_set = y - h

    for legend, s in (('tp_set', tp_set), ('fp_set', fp_set), ('fn_set', fn_set)):
        with open('_{}.txt'.format(legend), 'w') as f:
            f.write('\n'.join(['{},{},{}'.format(r_id, start, end) for r_id, start, end in sorted(s)]))

    tp = float(len(tp_set))
    fp = float(len(fp_set))
    fn = float(len(fn_set))

    # precision
    p = tp / (tp + fp)
    # recall
    r = tp / (tp + fn)
    # F1
    f1 = (2.0 * p * r) / (p + r)

    return p, r, f1


def export_test_data(x, y, file_prefix):
    s = u'\n'.join([u'{}<SPLITTER>{}'.format(review_id, review_text) for review_id, review_text in x])
    write_text(s, '{}_texts.txt'.format(file_prefix))
    s = '\n'.join(['{},{},{}'.format(review_id, start, end) for review_id, start, end in y])
    with open('{}_answers.csv'.format(file_prefix), 'w') as f:
        f.write(s)


def get_num_from_cat(cat):
    return CATEGORIES_REVERSE[cat]


def get_cat_from_num(num):
    return CATEGORIES[num]


def get_tags(text):
    """
    Вспомогательная функция для разметки текста с помощью TreeTagger'а (в формате по умолчанию).
    :param text: юникод-строка на русском языке
    :return: список ['словоформа\tтэг\tлемма', ...]
    """
    tags = tagger.TagText(text)
    return tags


def learn_terms(file_name):
    """Извлекает размеченные термины из файла xml.
    Нормализация терминов: нижний регистр (пока без лемматизации).
    Выход: словарь, где ключи - номера категорий (1-5), а значения -
    множества терминов, относящихся к данной категории;
    каждый термин состоит из одного или более слов."""
    tree = ET.parse(file_name)
    root = tree.getroot()
    terms_dict = {1: set(), 2: set(), 3: set(), 4: set(), 5: set()}
    for aspect in root.iter('aspect'):
        term = aspect.get('term').lower()
        cat = aspect.get('category')
        cat_num = get_num_from_cat(cat)
        terms_dict[cat_num].add(term)
    return terms_dict


def load_h(file_name):
    """
    Загружает гипотетические ответы без прогона всей системы.
    :param file_name: имя файла TXT
    :return: множество кортежей (text_id, term_start, term_end)
    """
    with open(file_name) as f:
        # h = set()
        exec(f.read())
    return h


def find_terms(text, terms):
    """
    Простой поиск терминов (без категорий).
    :param text: строка
    :param terms: множество терминов
    :return: список [(нач_позиция, кон_позиция, термин), ...]
    """
    text = text.lower()
    terms_found = []
    for term in terms:
        p_str = ur'\b{}\b'.format(term)
        p = re.compile(p_str, re.UNICODE)
        iterator = p.finditer(text)
        for m in iterator:
            terms_found.append((m.start(), m.end(), term))
    return terms_found


def find_terms_by_cat(text, terms_dict):
    """Простой поиск терминов, относящихся к 5 категориям.
    Выход:
        словарь {ном.катег.: список}, где
        каждый список имеет вид [(нач_позиция, кон_позиция, термин), ...]
    """
    text = text.lower()
    terms_found = {1: [], 2: [], 3: [], 4: [], 5: []}
    for cat, terms in terms_dict.items():
        for term in terms:
            p_str = ur'\b{}\b'.format(term)
            p = re.compile(p_str, re.UNICODE)
            iterator = p.finditer(text)
            for m in iterator:
                terms_found[cat].append((m.start(), m.end(), term))
    return terms_found


def find_terms_in_collection(collection, terms):
    """
    Простой поиск терминов (без категорий) для коллекции.
    :param collection: [(review_id, review_text), ...]
    :param terms: множество терминов
    :return: множество {[(review_id, start, end), ...]}
    """
    terms_found = set()
    n_terms = len(terms)
    i = 0
    for term in terms:
        i += 1
        print('looking for term', i, '/', n_terms)
        p_str = ur'\b{}\b'.format(term)
        p = re.compile(p_str, re.UNICODE)
        for text_id, text in collection:
            text = text.lower()
            iterator = p.finditer(text)
            for m in iterator:
                terms_found.add((text_id, m.start(), m.end()))
    return terms_found


def flatten_term_dict(terms_dict):
    """
    Преобразует словарь множеств во множество (игнорируя категории).
    :param terms_dict:
        словарь, где ключи - номера категорий (1-5), а значения -
        множества терминов, относящихся к данной категории
    :return: множество терминов
    """
    flat = set()
    for terms in terms_dict.values():
        flat = flat | terms
    return flat


def parse_test_data(file_name):
    """Извлекает тестовые данные (тексты отзывов и позиции аспектных терминов) из файла XML.
    :param file_name: имя файла XML с тестовыми данными
    :return: списки X и Y
    список X: [("review_id", "review_text"), ...]  <- тексты отзывов
    список Y: [("review_id", "start", "end"), ...]  <- позиции терминов
    Для каждого тестового файла функцию необходимо вызывать только один раз.
    Полученный список лучше эскпортировать в другой файл, напр. в формате .csv
    для дальнейшего использования, поскольку парсинг XML медленный.
    """
    tree = ET.parse(file_name)
    root = tree.getroot()
    x = []
    y = []
    for review in root.iter('review'):
        review_id = review.get('id')
        review_text = review.find('text').text
        x.append((review_id, review_text))
        for aspect in review.iter('aspect'):
            start = aspect.get('from')
            end = aspect.get('to')
            y.append((review_id, start, end))
    return x, y


def print_results(p, r, f):
    """Вспомогательная функция для вывода результатов."""
    res_tup = (('Precision', p), ('Recall', r), ('F1', f))
    for legend, res in res_tup:
        print(round(res, 4), legend)


def print_terms(terms_dict, with_positions=False, **kwargs):
    """
    Вспомогательная функция для вывода (экспорта) аспектных терминов в удобочитаемом виде.
    :param terms_dict: словарь {ном.катег.: список}, где
    каждый список имеет вид [(нач_позиция, кон_позиция, термин), ...]
    :param with_positions: выводить позиции в тексте?
    :param kwargs: для передачи в функцию print (например, для сохранения в файл)
    :return: None
    """
    output = []
    for num, terms in terms_dict.items():
        cat = get_cat_from_num(num)
        if with_positions:
            lines = [u'{}-{} {}'.format(start, end, t) for start, end, t in sorted(terms)]
            terms_str = u'\n'.join(lines)
        else:
            terms_str = u'\n'.join(sorted(list(terms)))
        output.append(u'\n\n***{}***\n{}'.format(cat, terms_str))
    print(u''.join(output), **kwargs)


def read_test_answers(file_name):
    """
    Считывает тестовые данные из файла .csv
    :param file_name: название файла .csv
    :return: множество Y: {("review_id", "start", "end"), ...}
    """
    y = set()
    with open(file_name) as f:
        for line in f:
            review_id, start, end = line.split(',')
            y.add((int(review_id), int(start), int(end)))
    return y


def read_test_reviews(file_name):
    """
    Возвращает тексты обзоров.
    :param file_name: напр. "_rest_test_texts.txt"
    :return: список [("review_id", "review_text"), ...]
    """
    x = []
    with codecs.open(file_name, 'r', 'utf-8') as f:
        for line in f:
            review_id, review_text = line.split('<SPLITTER>')
            x.append((int(review_id), review_text))
    return x


def remove_subterms(h):
    """
    Убирает "субтермины" (короткие термины в составе длинных).
    :param h: множество кортежей (text_id, term_start, term_end)
    :return: сокращённое множество кортежей (text_id, term_start, term_end)
    """
    term_list = sorted(h)
    n_old = len(term_list)
    i = j = last_end = 0
    new_h = set()
    while True:
        j += 1
        curr_id, curr_start, curr_end = term_list[i]
        if j < n_old:
            next_id, next_start, next_end = term_list[j]
        else:
            # we are about to finish the loop; should we add the current term?
            if curr_end > last_end:
                new_h.add((curr_id, curr_start, curr_end))
            break
        # are we in the same text?
        if next_id == curr_id:
            # do we have an overlap of terms? is this term's end position larger than that of the last term added?
            if next_start > curr_start and curr_end > last_end:
                # if yes, add current term
                new_h.add((curr_id, curr_start, curr_end))
                last_end = curr_end  # memorize end position of last added term
        else:
            # new text is about to start; should we add the current term?
            if curr_end > last_end:
                new_h.add((curr_id, curr_start, curr_end))
            # clear the last_end
            last_end = 0
        # advance
        i += 1
    n_new = len(new_h)
    n_removed = n_old - n_new
    print('removed {} subterms ({} -> {})'.format(n_removed, n_old, n_new))
    return new_h


def submit_results(h, target_file):
    """
    Форматирует гипотетические ответы в XML для подачи на конкурс.
    :param h: множество кортежей (text_id, start, end)
    :param target_file: название выходного файла XML
    :return: None (сохраняет XML файл)
    """
    if target_file == 'SentiRuEval_rest_test_final.xml':  # some defensive coding here
        raise Exception, "Bad target file name"
    curr_id = 0
    h_dict = {}
    for r_id, start, end in sorted(h):
        if r_id in h_dict:
            h_dict[r_id].append((start, end))
        else:
            h_dict[r_id] = [(start, end)]
    print('h_dict is ready')
    x = read_test_reviews("_rest_test_texts.txt")
    review_dict = {r_id: r_text for r_id, r_text in x}
    print('review_dict is ready')
    terms_w_quotes = 0
    with codecs.open("SentiRuEval_rest_test_final.xml", 'r', 'utf-8') as source, codecs.open(target_file, 'w', 'utf-8') as target:
        for line in source.readlines():
            target.write(line)
            if '<review id=' in line:
                curr_id = int(line.split('"')[1])
                print('writing', curr_id)
            if '</aspects>' in line and curr_id in h_dict:
                target.write('\t\t<aspects1>\n')
                for start, end in h_dict[curr_id]:
                    term = review_dict[curr_id][start:end]
                    if u'"' in term:
                        terms_w_quotes += 1
                    term = term.replace(u'"', u'&quot;')  # more defensive coding
                    target.write(u'\t\t\t<aspect category="" from="{}" mark="Rel" sentiment="" term="{}" '
                                 u'to="{}" type=""/>\n'.format(start, term, end))
                target.write('\t\t</aspects1>\n')
    print('Done writing.')
    print('Terms with quotes found: ', terms_w_quotes)


def write_text(text, file_name):
    """
    Вспомогательная функция для сохранения текста (=строки) в файл
    :param text: строка
    :param file_name: имя файла
    :return: None
    """
    target_file = codecs.open(file_name, 'w', 'utf-8')
    target_file.write(text)
    target_file.close()


def write_term_tuples(h, file_name):
    """
    Сохраняет на диск гипотетические ответы в удобочитаемом виде
    :param h: множество кортежей (text_id, start, end)
    :param file_name: имя файла
    :return: None
    """
    with open(file_name, 'w') as f:
        f.write('\n'.join([str(line) for line in sorted(h)]))


if __name__ == '__main__':
    h = load_h('_h_base_super on test set.txt')
    submit_results(h, '_submit_temp.xml')
    # annotate('SentiRuEval_rest_markup.xml', '_toy_test_texts.txt', '_toy_test_answers.csv')
    # annotate('SentiRuEval_rest_markup.xml', '_rest_test_texts.txt', '_rest_test_answers.csv')

